package ru.tsc.ichaplygina.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.bootstrap.Bootstrap;
import ru.tsc.ichaplygina.taskmanager.command.domain.DomainLoadBackupCommand;
import ru.tsc.ichaplygina.taskmanager.command.domain.DomainSaveBackupCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private static final String LOAD_BACKUP_COMMAND = DomainLoadBackupCommand.NAME;

    @NotNull
    private static final String SAVE_BACKUP_COMMAND = DomainSaveBackupCommand.NAME;

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.propertyService = propertyService;
    }

    public void init() {
        load();
        start();
    }

    private void load() {
        bootstrap.executeCommand(LOAD_BACKUP_COMMAND);
    }

    @Override
    @SneakyThrows
    public void run() {
        save();
    }

    private void save() {
        bootstrap.executeCommand(SAVE_BACKUP_COMMAND);
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, propertyService.getAutosaveFrequency(), TimeUnit.MILLISECONDS);
    }

}

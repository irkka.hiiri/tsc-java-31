package ru.tsc.ichaplygina.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.bootstrap.Bootstrap;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner implements Runnable {

    @Nullable
    private List<String> commands;

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ICommandService commandService;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService, @NotNull final ICommandService commandService) {
        this.bootstrap = bootstrap;
        this.commandService = commandService;
        this.propertyService = propertyService;
    }

    @NotNull
    private List<String> getCommands() {
        return commandService.getArguments().values()
                .stream()
                .map(AbstractCommand::getCommand)
                .collect(Collectors.toList());
    }

    public void init() {
        commands = getCommands();
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        if (commands == null) return;
        @NotNull final String path = propertyService.getFileScannerPath();
        @NotNull final List<String> files = Files.list(Paths.get(path))
                .filter(file -> !Files.isDirectory(file))
                .map(Path::getFileName)
                .map(Path::toString)
                .collect(Collectors.toList());
        for (final String fileName : files) {
            if (commands.contains(fileName)) {
                bootstrap.executeCommand(fileName);
                System.out.println(fileName);
                Files.delete(Paths.get(fileName));
            }
        }
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, propertyService.getFileScannerFrequency(), TimeUnit.MILLISECONDS);
    }
}

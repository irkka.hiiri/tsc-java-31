package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.dto.Domain;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DomainSaveJsonJaxbCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save json jaxb";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to json file using jaxb";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_JSON);
        marshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}

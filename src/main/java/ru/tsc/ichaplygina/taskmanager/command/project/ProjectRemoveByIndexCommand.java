package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "remove project by index";

    @NotNull
    public final static String DESCRIPTION = "remove project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getProjectTaskService().removeProjectByIndex(index - 1));
    }

}

package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printList;

public final class UserListCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "list users";

    @NotNull
    public static final String DESCRIPTION = "show all users";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        if (getUserService().isEmpty()) {
            printLinesWithEmptyLine("No users found!");
            return;
        }
        printList(getUserService().findAll());
    }

}
